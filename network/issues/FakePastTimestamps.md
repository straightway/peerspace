# Fake past timestamps

Malicious peers may pretend to have posted a data chunk at an earlier
time in the past by simply specifying an according time stamp.