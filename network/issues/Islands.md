# Network islands

Parts of the network may be (temporarily) completely disconnected
from other parts, i.e. there is no route of known peers from any peer in the one
partition to any peer in the other one. The probablity for this situation must be lowered
as much as possible by tuning the network parameters and algorithms accordingly (e.g.
the number of known peers stored by every peer).
