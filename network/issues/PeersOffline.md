# Peers go online and offline

A general problem is that peers go online and offline as they like. Consequently, any peer
might not be reachable at any time. The network must be robust against this situation, because
it cannot be avoided. The solution is redundant storage of data on several peers and raise the
probability to reach one that has the desired chunk.
