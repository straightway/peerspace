# Redundancy vs. capacity

To be robust against failed queries due to (temporary) offline peers, data must be stored
redundantly on multiple peers. As the overall storage capacity of the Peerspace network is limited,
redundancy further reduces this capacity. So a trade-off between redundancy and capacity must be
made by a smart distribution algorithm.

See also: [Peers are offline](PeersOffline.md)
