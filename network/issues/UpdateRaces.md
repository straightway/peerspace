# Update races for mutable chunks

Several peers may update a mutable data chunk again and again in order to override updates
made by other peers showing the same behavior.