/*
 * Copyright 2016 straightway
 *
 *  Licensed under the Apache License, Version 2.0 (the &quot;License&quot;);
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an &quot;AS IS&quot; BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.peerspace.net

import java.io.Serializable
import org.junit.jupiter.api.Test
import straightway.testing.bdd.Given
import straightway.testing.flow.Same
import straightway.testing.flow.as_
import straightway.testing.flow.expect
import straightway.testing.flow.is_

class ChannelTest {
    private class SUT : Channel {
        var calledResultListener: TransmissionResultListener? = null
        override fun transmit(data: Serializable, resultListener: TransmissionResultListener) {
            calledResultListener = resultListener
        }
    }

    @Test
    fun `result listener is ignored if not specified`() =
            Given {
                SUT()
            } when_ {
                transmit("data")
            } then {
                expect(calledResultListener is_ Same as_ TransmissionResultListener.Ignore)
            }
}