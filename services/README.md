# Straightway.Peerspace Services

Services provide higher level functionality based on the [network](../network/README.md) or on
the [transport](../transport/README.md) layer.

For the following services, a draft concept exists: 

- [Messaging](messaging/README.md)
- [Searching](searching/README.md)