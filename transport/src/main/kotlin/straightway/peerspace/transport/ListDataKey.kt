/*
 * Copyright 2016 straightway
 *
 *  Licensed under the Apache License, Version 2.0 (the &quot;License&quot;);
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an &quot;AS IS&quot; BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.peerspace.transport

import java.time.LocalDateTime
import straightway.peerspace.data.Id
import straightway.peerspace.data.Key
import straightway.units.absolute
import straightway.units.get
import straightway.units.milli
import straightway.units.second

/**
 * Key for a data item being part of a peerspace list.
 */
data class ListDataKey(val listId: Id, val timestamp: LocalDateTime)

fun Key.toListDataKey() = ListDataKey(id, timestamp[milli(second)].absolute)