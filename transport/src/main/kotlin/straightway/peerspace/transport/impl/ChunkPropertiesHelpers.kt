/*
 * Copyright 2016 straightway
 *
 *  Licensed under the Apache License, Version 2.0 (the &quot;License&quot;);
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an &quot;AS IS&quot; BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.peerspace.transport.impl

import straightway.peerspace.transport.ChunkProperties

val ChunkProperties.cryptoContainerPayloadSizeBytes get() =
    chunkSizeBytes - cryptoContainerHeaderSizeBytes

val ChunkProperties.cryptoContainerPayloadSizeRespectingBlockSizeBytes get() =
    (cryptoContainerPayloadSizeBytes / dataBlockSizeBytes) * dataBlockSizeBytes

val ChunkProperties.maxVersion2PayloadSizeInCryptoContainerBytes get() =
    cryptoContainerPayloadSizeRespectingBlockSizeBytes - DataChunkVersion2.Header.MIN_SIZE

val ChunkProperties.version0PayloadSizeInCryptoContainerBytes get() =
    cryptoContainerPayloadSizeRespectingBlockSizeBytes - DataChunkVersion0.Header.SIZE

fun ChunkProperties.getMaxVersion2PayloadSizeWithReferencesInCryptoContainerBytes(
    numberOfReferences: Int
) = cryptoContainerPayloadSizeRespectingBlockSizeBytes - DataChunkVersion2.Header.MIN_SIZE -
        numberOfReferences * referenceBlockSizeBytes
