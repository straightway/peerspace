/*
 * Copyright 2016 straightway
 *
 *  Licensed under the Apache License, Version 2.0 (the &quot;License&quot;);
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an &quot;AS IS&quot; BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.peerspace.transport.impl

import straightway.peerspace.transport.ChunkProperties

/**
 * Create a plain chunk from given properties and a given data array.
 */
class PlainChunkCreator(
    private val chunkProperties: ChunkProperties,
    private val data: ByteArray
) {

    val plainChunkStructure: DataChunkStructure by lazy<DataChunkStructure> {
        when {
            chunkProperties.version0PayloadSizeInCryptoContainerBytes == data.size ->
                DataChunkVersion0(data)
            chunkProperties.maxVersion2PayloadSizeInCryptoContainerBytes < data.size ->
                DataChunkVersion1(data, additionalVersion1PayloadBytes)
            else ->
                createPlainVersion2Chunk()
        }
    }

    // region Private

    private val additionalVersion1PayloadBytes: Int get() =
            chunkProperties.cryptoContainerPayloadSizeRespectingBlockSizeBytes -
                DataChunkVersion1.Header.SIZE -
                data.size

    private fun createPlainVersion2Chunk(): DataChunkVersion2 =
            DataChunkVersion2Builder(
                    chunkProperties.cryptoContainerPayloadSizeRespectingBlockSizeBytes
            ).also {
                it.payload = data
            }.chunkStructure

    // endregion
}