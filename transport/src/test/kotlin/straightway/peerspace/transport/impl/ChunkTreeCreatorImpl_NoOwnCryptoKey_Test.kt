/*
 * Copyright 2016 straightway
 *
 *  Licensed under the Apache License, Version 2.0 (the &quot;License&quot;);
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an &quot;AS IS&quot; BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.peerspace.transport.impl

import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import straightway.koinutils.KoinLoggingDisabler
import straightway.peerspace.transport.ChunkProperties
import straightway.peerspace.transport.ChunkTreeInfo
import straightway.peerspace.transport.ChunkerCrypto
import straightway.peerspace.transport.createChunkTreeCreator
import straightway.peerspace.transport.trace
import straightway.testing.TestTraceProvider
import straightway.testing.TraceOnFailure
import straightway.testing.bdd.Given
import straightway.testing.flow.Equal
import straightway.testing.flow.expect
import straightway.testing.flow.is_
import straightway.testing.flow.to_
import straightway.utils.TraceProvider
import straightway.utils.Tracer

@ExtendWith(TraceOnFailure::class)
class ChunkTreeCreatorImpl_NoOwnCryptoKey_Test : KoinLoggingDisabler(), TestTraceProvider {

    private companion object {
        const val chunkSizeBytes = 0x20
        const val maxReferences = 2
        const val version0PayloadSize = chunkSizeBytes - 2 * DataChunkVersion0.Header.SIZE
    }

    private var testTrace: Tracer? = null

    private fun test(
        cryptoBlockSize: Int = 1,
        dataToChopToChunkGetter: ChunkEnvironmentValues.() -> ByteArray
    ) =
            Given {
                val result = object {
                    val treeInfos = mutableMapOf<Int, ChunkTreeInfo>()
                    val env = ChunkingTestEnvironment(
                            chunkSizeBytes,
                            maxReferences,
                            cryptoBlockSize,
                            dataToChopToChunkGetter,
                            treeInfoFactory = { _, size -> context.trace(size) { treeInfos[size]!! } },
                            tracerFactory = { Tracer { true } })
                    var crypto = ChunkerCrypto.forPlainChunk(env.cryptor)
                    val sut by lazy {
                        env.env.context.createChunkTreeCreator(env.data, crypto, ChunkProperties(
                                chunkSizeBytes = env.chunkSizeBytes,
                                maxReferences = 2,
                                referenceBlockSizeBytes = 0,
                                dataBlockSizeBytes = cryptoBlockSize,
                                cryptoContainerHeaderSizeBytes = DataChunkVersion0.Header.SIZE
                        ))
                    }
                }

                testTrace = result.env.env.context.trace

                result
            }

    override val traces: Collection<String> get() = (testTrace as TraceProvider).traces
            .map { it.toString().replace("straightway.peerspace.transport.impl.", "") }

    @BeforeEach
    fun setUp() {
        testTrace = null
    }

    @Test
    fun `chunk fitting exactly into version 0 single data structure`() =
            test {
                ByteArray(version0PayloadSize) { it.toByte() }
            } while_ {
                with(env) {
                    cryptor = negatingEncryptor
                    crypto = ChunkerCrypto.forPlainChunk(cryptor)
                    data.createPlainDataChunkVersion0().end()
                }
            } when_ {
                sut.chunks
            } then {
                env.assertExpectedChunks(it.result)
            }

    @Test
    fun `chunk smaller than version 1 single data structure`() =
            test {
                byteArrayOf(0x33)
            } while_ {
                with(env) {
                    data.createPlainDataChunkVersion2().end()
                }
            } when_ {
                sut.chunks
            } then {
                env.assertExpectedChunks(it.result)
            }

    @Test
    fun `chunk smaller than version 0 but larger than version 2 single data structure`() =
            test {
                ByteArray(version0PayloadSize - 2) { it.toByte() }
            } while_ {
                with(env) {
                    data.createPlainDataChunkVersion1(1).end()
                }
            } when_ {
                sut.chunks
            } then {
                env.assertExpectedChunks(it.result)
            }

    @Test
    fun `chunk larger than version 0 chunk tree`() =
            test {
                ByteArray(version0PayloadSize + 1) { it.toByte() }
            } while_ {
                with(env) {
                    val directoryPayloadSizeBytes = maxChunkVersion2PayloadSizeWithReferences(1)
                    treeInfos[data.size] = ChunkTreeInfo(version0PayloadSize, directoryPayloadSizeBytes)
                    data.reserveForDirectory(directoryPayloadSizeBytes)
                            .createPlainDataChunkVersion2()
                            .createDirectoryDataChunkWithNumberOfReferences(1)
                            .end()
                }
            } when_ {
                sut.chunks
            } then {
                env.assertExpectedChunks(it.result)
            }

    @Test
    fun `chunk requiring two sub chunks`() =
            test {
                ByteArray(maxChunkVersion2PayloadSizeWithReferences(1) + version0PayloadSize + 1) { it.toByte() }
            } while_ {
                with(env) {
                    val directoryPayloadSizeBytes = maxChunkVersion2PayloadSizeWithReferences(2)
                    treeInfos[data.size] = ChunkTreeInfo(version0PayloadSize, directoryPayloadSizeBytes)
                    data.reserveForDirectory(directoryPayloadSizeBytes)
                            .createPlainDataChunkVersion0()
                            .createPlainDataChunkVersion2()
                            .createDirectoryDataChunkWithNumberOfReferences(2)
                            .end()
                }
            } when_ {
                sut.chunks
            } then {
                env.assertExpectedChunks(it.result)
            }

    @Test
    fun `block size leading to padding with random bytes`() =
            test(cryptoBlockSize = 0x8) {
                byteArrayOf(0x33)
            } while_ {
                with(env) {
                    data.createPlainDataChunkVersion2().end()
                }
            } when_ {
                sut.chunks
            } then {
                env.assertExpectedChunks(it.result)
            }

    @Test
    fun `small plain data results in single chunk version 2`() =
            test {
                byteArrayOf()
            } while_ {
                with(env) {
                    data.createPlainDataChunkVersion2().end()
                }
            } when_ {
                sut.chunks
            } then {
                env.assertExpectedChunks(it.result)
            }

    @Test
    fun `data is encrypted using passed encryptor`() =
            test {
                byteArrayOf()
            } while_ {
                with(env) {
                    data.createPlainDataChunkVersion2().end()
                }
            } when_ {
                sut.chunks
            } then {
                env.assertExpectedChunks(it.result)
            }

    @Test
    fun `plain data of exact version 0 payload size produces version 0 chunk`() =
            test {
                ByteArray(version0PayloadSize) { it.toByte() }
            } while_ {
                with(env) {
                    data.createPlainDataChunkVersion0().end()
                }
            } when_ {
                sut.chunks
            } then {
                env.assertExpectedChunks(it.result)
            }

    @Test
    fun `plain data between version 0 and version 2 payload size produces version 1 chunk`() =
            test {
                ByteArray(version0PayloadSize - 2) { it.toByte() }
            } while_ {
                with(env) {
                    data.createPlainDataChunkVersion1(1).end()
                }
            } when_ {
                sut.chunks
            } then {
                env.assertExpectedChunks(it.result)
            }

    @Test
    fun `plain data chunk id is equal to hash of chunk's binary data`() =
            test {
                byteArrayOf(1, 2, 3)
            } while_ {
                with(env) {
                    data.createPlainDataChunkVersion2().end()
                }
            } when_ {
                sut.chunks
            } then {
                expect(it.result.single().key is_ Equal
                    to_ env.hashKey(env.setUpChunks.single()))
            }


    @Test
    fun `plain data larger than chunk size results in two chunks`() =
            test {
                ByteArray(version0PayloadSize + 1) { it.toByte() }
            } while_ {
                with(env) {
                    treeInfos[version0PayloadSize + 1] = ChunkTreeInfo(
                        maxSubTreeSizeBytes = version0PayloadSize,
                        directoryPayloadSizeBytes = maxChunkVersion2PayloadSizeWithReferences(1))
                    data.reserveForDirectory(maxChunkVersion2PayloadSizeWithReferences(1))
                        .createPlainDataChunkVersion2()
                        .createDirectoryDataChunkWithNumberOfReferences(1)
                        .end()
                }
            } when_ {
                sut.chunks
            } then {
                env.assertExpectedChunks(it.result)
            }

    @Test
    fun `plain data larger than twice the chunk size results in three chunks`() =
        test {
            ByteArray(2 * version0PayloadSize + 1) { it.toByte() }
        } while_ {
            with(env) {
                treeInfos[2 * version0PayloadSize + 1] = ChunkTreeInfo(
                    maxSubTreeSizeBytes = version0PayloadSize,
                    directoryPayloadSizeBytes = maxChunkVersion2PayloadSizeWithReferences(2))
                data.reserveForDirectory(maxChunkVersion2PayloadSizeWithReferences(2))
                    .createPlainDataChunkVersion0()
                    .createPlainDataChunkVersion2()
                    .createDirectoryDataChunkWithNumberOfReferences(2)
                    .end()
            }
        } when_ {
            sut.chunks
        } then {
            env.assertExpectedChunks(it.result)
        }

    @Test
    fun `plain data larger than three times the chunk size results in hierarchical chunks`() =
        test {
            ByteArray(3 * version0PayloadSize + unencryptedChunkSizeBytes / 2) { it.toByte() }
        } while_ {
            with (env) {
                treeInfos[3 * version0PayloadSize + unencryptedChunkSizeBytes / 2] = ChunkTreeInfo(
                    maxSubTreeSizeBytes = 2 * version0PayloadSize + maxChunkVersion2PayloadSizeWithReferences(2),
                    directoryPayloadSizeBytes = maxChunkVersion2PayloadSizeWithReferences(2))
                treeInfos[2 * version0PayloadSize + maxChunkVersion2PayloadSizeWithReferences(2)] = ChunkTreeInfo(
                    maxSubTreeSizeBytes = version0PayloadSize,
                    directoryPayloadSizeBytes = maxChunkVersion2PayloadSizeWithReferences(2))
                data.reserveForDirectory(maxChunkVersion2PayloadSizeWithReferences(2))
                    .reserveForDirectory(maxChunkVersion2PayloadSizeWithReferences(2))
                    .createPlainDataChunkVersion0()
                    .createPlainDataChunkVersion0()
                    .createDirectoryDataChunkWithNumberOfReferences(2)
                    .createPlainDataChunkVersion2()
                    .createDirectoryDataChunkWithNumberOfReferences(2)
                    .end()
            }
        } when_ {
            sut.chunks
        } then {
            env.assertExpectedChunks(it.result)
        }
}