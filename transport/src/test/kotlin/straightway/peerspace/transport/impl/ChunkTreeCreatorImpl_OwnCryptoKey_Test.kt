/*
 * Copyright 2016 straightway
 *
 *  Licensed under the Apache License, Version 2.0 (the &quot;License&quot;);
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an &quot;AS IS&quot; BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.peerspace.transport.impl

import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import straightway.koinutils.KoinLoggingDisabler
import straightway.peerspace.transport.ChunkProperties
import straightway.peerspace.transport.ChunkTreeInfo
import straightway.peerspace.transport.ChunkerCrypto
import straightway.peerspace.transport.createChunkTreeCreator
import straightway.peerspace.transport.trace
import straightway.testing.TestTraceProvider
import straightway.testing.TraceOnFailure
import straightway.testing.bdd.Given
import straightway.utils.TraceProvider
import straightway.utils.Tracer

@ExtendWith(TraceOnFailure::class)
class ChunkTreeCreatorImpl_OwnCryptoKey_Test : KoinLoggingDisabler(), TestTraceProvider {

    private companion object {
        const val chunkSizeBytes = 0x20
        const val maxReferences = 2
        const val version0PayloadSize = chunkSizeBytes - 2 * DataChunkVersion0.Header.SIZE
    }

    private var testTrace: Tracer? = null

    private fun test(
        cryptoBlockSize: Int = 1,
        dataToChopToChunkGetter: ChunkEnvironmentValues.() -> ByteArray
    ) =
            Given {
                val result = object {
                    val treeInfos = mutableMapOf<Int, ChunkTreeInfo>()
                    val env = ChunkingTestEnvironment(
                            chunkSizeBytes,
                            maxReferences,
                            cryptoBlockSize,
                            dataToChopToChunkGetter,
                            treeInfoFactory = { _, size -> treeInfos[size]!! },
                            tracerFactory = { Tracer { true } })
                    var crypto = ChunkerCrypto.forPlainChunk(env.cryptor)
                    val sut by lazy {
                        env.env.context.createChunkTreeCreator(env.data, crypto, ChunkProperties(
                                chunkSizeBytes = env.chunkSizeBytes,
                                maxReferences = 2,
                                referenceBlockSizeBytes = 0,
                                dataBlockSizeBytes = cryptoBlockSize,
                                cryptoContainerHeaderSizeBytes = DataChunkVersion0.Header.SIZE
                        ))
                    }
                }

                testTrace = result.env.env.context.trace

                result
            }

    override val traces: Collection<String> get() = (testTrace as TraceProvider).traces
            .map { it.toString().replace("straightway.peerspace.transport.impl.", "") }

    @BeforeEach
    fun setUp() {
        testTrace = null
    }

    @Test
    fun `plain version 2 chunk with own crypto key`() =
            test {
                ByteArray(9) { it.toByte() }
            } while_ {
                with(env) {
                    cryptor = notEncryptor
                    encryptorProperties.maxClearTextBytes = 8
                    crypto = ChunkerCrypto.forPlainChunk(notEncryptor)
                    data.createEncryptedPlainDataChunkVersion2(newSymmetricCryptor).end()
                }
            } when_ {
                sut.chunks
            } then {
                env.assertExpectedChunks(it.result)
            }
}