/*
 * Copyright 2016 straightway
 *
 *  Licensed under the Apache License, Version 2.0 (the &quot;License&quot;);
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an &quot;AS IS&quot; BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.peerspace.transport.impl

import org.junit.jupiter.api.Test
import straightway.peerspace.transport.ChunkProperties
import straightway.testing.bdd.Given
import straightway.testing.flow.Equal
import straightway.testing.flow.expect
import straightway.testing.flow.is_
import straightway.testing.flow.to_

class PlainChunkCreatorTest {

    private companion object {
        val chunkProperties = ChunkProperties(
                chunkSizeBytes = 0x10,
                dataBlockSizeBytes = 1,
                maxReferences = 2,
                referenceBlockSizeBytes = 4,
                cryptoContainerHeaderSizeBytes = 3)
    }

    @Test
    fun `data exactly fits into version 0 chunk`() =
            Given {
                ByteArray(
                        chunkProperties.version0PayloadSizeInCryptoContainerBytes
                ) { it.toByte() }
            } when_ {
                PlainChunkCreator(chunkProperties, this).plainChunkStructure
            } then {
                expect(it.result is_ Equal to_ DataChunkVersion0(this))
            }

    @Test
    fun `data too large for version 2 but smaller than version 0 chunk`() =
            Given {
                ByteArray(
                        chunkProperties.version0PayloadSizeInCryptoContainerBytes - 1
                ) { it.toByte() }
            } when_ {
                PlainChunkCreator(chunkProperties, this).plainChunkStructure
            } then {
                expect(it.result is_ Equal to_ DataChunkVersion1(this, 0))
            }

    @Test
    fun `data too large for version 2 but smaller than version 0 chunk with offset`() =
            Given {
                ByteArray(
                        chunkProperties.version0PayloadSizeInCryptoContainerBytes - 2
                ) { it.toByte() }
            } when_ {
                PlainChunkCreator(chunkProperties, this).plainChunkStructure
            } then {
                expect(it.result is_ Equal to_ DataChunkVersion1(this, 1))
            }

    @Test
    fun `data fits into version 2`() =
            Given {
                ByteArray(
                        chunkProperties.maxVersion2PayloadSizeInCryptoContainerBytes
                ) { it.toByte() }
            } when_ {
                PlainChunkCreator(chunkProperties, this).plainChunkStructure
            } then {
                val data = this
                expect(it.result is_ Equal to_
                        DataChunkVersion2Builder(chunkProperties.chunkSizeBytes).apply {
                            payload = data
                        }.chunkStructure)
            }
}